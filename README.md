# OLAT - Online Learning and Training

[![License](https://img.shields.io/badge/License-Apache--2.0-blue)](https://www.apache.org/licenses/LICENSE-2.0)
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](CODE_OF_CONDUCT.md)
[![OpenOlat](https://img.shields.io/badge/OpenOlat%20Starter-18.2--SNAPSHOT-1aa6be)](https://gitlab.com/olatorg/openolat-starter)
[![Spring Boot](https://img.shields.io/badge/Spring_Boot-3.2.11-6bb536)](https://docs.spring.io/spring-boot/docs/3.2.11/reference/html/)

**OLAT** is an acronym for **O**nline **L**earning **A**nd **T**raining. It is a
mature [Learning Management System](https://en.wikipedia.org/wiki/Learning_management_system) and
has been developed since 1999 at the [University of Zurich](https://www.uzh.ch/).

## Docker

OLAT provides [Docker images](https://gitlab.com/olatorg/olat-oss/container_registry/) and
configuration for [Docker Compose](https://docs.docker.com/compose/).

Use the following commands to start OLAT locally.

1. Create a `.env` file to pass environment variables.

   ```shell
   cat > .env<< EOF
   TAG=<tag>
   EOF
   ```

2. Start Docker containers.

   ```shell
   docker compose up --detach
   ```

3. View log files of running OLAT container.

   ```shell
   docker compose logs app --follow
   ```

4. Stop and remove containers and volumes

   ```shell
   docker compose down --volumes
   ```

## License

This project is Open Source software released under
the [Apache 2.0 license](https://www.apache.org/licenses/LICENSE-2.0).
