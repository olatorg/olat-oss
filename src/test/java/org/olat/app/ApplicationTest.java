/*
 * Copyright 2023 OLAT (olatorg)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.olat.app;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

/**
 * @author Christian Schweizer
 * @since 1.0
 */
@SpringBootTest(
    properties = "openolat.properties.userdata.dir=${java.io.tmpdir}${random.value}",
    webEnvironment = WebEnvironment.RANDOM_PORT)
@Testcontainers
class ApplicationTest {

  @Container
  static PostgreSQLContainer<?> POSTGRES = new PostgreSQLContainer<>("postgres:12-alpine");

  @DynamicPropertySource
  static void dbProperties(DynamicPropertyRegistry registry) {
    registry.add("openolat.properties.db.host", () -> POSTGRES.getHost());
    registry.add("openolat.properties.db.host.port", () -> POSTGRES.getFirstMappedPort());
    registry.add("openolat.properties.db.name", () -> POSTGRES.getDatabaseName());
    registry.add("openolat.properties.db.user", () -> POSTGRES.getUsername());
    registry.add("openolat.properties.db.pass", () -> POSTGRES.getPassword());
  }

  @Test
  @SuppressWarnings("java:S2699")
  void contextLoads() {}
}
